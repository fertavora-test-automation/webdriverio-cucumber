# WebdriverIO 5 - Cucumber

An example on how to implement Webdriverio 5 and Cucumber

## Instructions
1. Clone this repo
2. Run `npm install` in the repo root directory
3. Create an `.env` file
4. Define the environment variables on the `.env` file created based on `.env.default` examples.
5. Start the Selenium Server, it is recommended to use [Zalenium](https://opensource.zalando.com/zalenium/ "Zalenium - A flexible and scalable Selenium Grid.").
6. Run `npm test`
