const { expect } = require('chai');
const { When } = require('cucumber');
const { Checkout, Overview } = require('../../pages');
const { ErrorMessages } = require('../../utils');

When(/^the user fills the checkout form$/, () => {
  Checkout.fillCheckoutForm('John', 'Doe', '90210');
  Checkout.clickContinueButton();
  expect(Overview.areOrderDetailsDisplayed()).to.equal(true, ErrorMessages.ORDER_DETAILS_NOT_DISPLAYED);
});
