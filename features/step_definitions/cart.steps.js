const { expect } = require('chai');
const { When } = require('cucumber');
const { Cart, Checkout } = require('../../pages');
const { ErrorMessages } = require('../../utils');

When(/^the user proceeds checkout$/, () => {
  Cart.clickCheckoutButton();
  expect(Checkout.isFormDisplayed()).to.equal(true, ErrorMessages.CHECKOUT_FORM_IS_NOT_DISPLAYED);
});
