const { expect } = require('chai');
const { ProductDetailsPage, Cart } = require('../../pages');

const { When } = require('cucumber');

When(/^the user add the product to the cart$/, () => {
  ProductDetailsPage.clickButtonAddToCart();
  expect(ProductDetailsPage.isRemoveButtonDisplayed()).to.equal(true);
  expect(ProductDetailsPage.isCartBadgeDisplayed()).to.equal(true);
});

When(/^the user goes to the cart page$/, () => {
  ProductDetailsPage.clickButtonGoToCart();
  expect(Cart.isContinueShoppingButtonDisplayed()).to.equal(true);
});
