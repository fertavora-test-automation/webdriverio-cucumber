const { expect } = require('chai');
const { Then } = require('cucumber');
const { Finish } = require('../../pages');
const { ErrorMessages } = require('../../utils');

Then(/^the Thank You message is displayed$/, () => {
  expect(Finish.getHeaderText()).to.equal('THANK YOU FOR YOUR ORDER', ErrorMessages.THANK_YOU_MESSAGE_IS_WRONG);
});
