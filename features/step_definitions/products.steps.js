const { expect } = require('chai');
const { ProductsPage, ProductDetailsPage } = require('../../pages');
const { When } = require('cucumber');

When(/^the user selects product (.*)$/, (product) => {
  ProductsPage.clickProductByText(product);
  expect(ProductDetailsPage.isProductDetailsDisplayed()).to.equal(true);
});
