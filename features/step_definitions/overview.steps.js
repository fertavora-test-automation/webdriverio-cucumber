const { expect } = require('chai');
const { When } = require('cucumber');
const { Overview, Finish } = require('../../pages');
const { ErrorMessages } = require('../../utils');

When(/^the user confirms the checkout overview$/, () => {
  Overview.clickFinishButton();
  expect(Finish.isThankYouMessageDisplayed()).to.equal(true, ErrorMessages.FINISH_PAGE_NOT_DISPLAYED)
});
