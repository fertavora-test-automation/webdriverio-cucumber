const { expect } = require('chai');
const { Given } = require('cucumber');
const { LoginPage, ProductsPage } = require('../../pages');

Given(/^the user is signed in$/, () => {
  browser.url('/');
  LoginPage.signIn(process.env.USERNAME, process.env.PASSWORD);
  expect(ProductsPage.areProductItemsDisplayed()).to.equal(true);
});
