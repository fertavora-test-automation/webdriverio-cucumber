Feature: Checkout
  As a customer
  I want to select and add a product to the cart
  so I can proceed checkout

  Scenario Outline: User selects '<productLabel>' product, adds to cart and proceeds checkout
    Given the user is signed in
    When the user selects product <productLabel>
    And the user add the product to the cart
    And the user goes to the cart page
    And the user proceeds checkout
    And the user fills the checkout form
    And the user confirms the checkout overview
    Then the Thank You message is displayed

  Examples:
  | productLabel                      |
  | Sauce Labs Backpack               |
  | Sauce Labs Bolt T-Shirt           |
  | Sauce Labs Onesie                 |
  | Sauce Labs Bike Light             |
  | Sauce Labs Fleece Jacket          |
  | Test.allTheThings() T-Shirt (Red) |
