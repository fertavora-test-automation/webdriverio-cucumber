const ErrorMessages = {
  PRODUCTS_NOT_DISPLAYED: 'Products list is not displayed!',
  PRODUCT_DETAILS_BACK_BUTTON_NOT_DISPLAYED: 'Product Details Back button is not displayed!',
  PRODUCT_DETAILS_ADD_TO_CART_BUTTON_NOT_DISPLAYED: 'Products Details Add To Cart button is not displayed!',
  CART_BADGE_NOT_DISPLAYED: 'Cart badge is not displayed, was product added to cart?',
  REMOVE_BUTTON_NOT_DISPLAYED: 'Remove button is not displayed, is product in the cart?',
  CONTINUE_SHOPPING_BUTTON_NOT_DISPLAYED: 'Continue Shopping button not displayed!',
  CHECKOUT_FORM_IS_NOT_DISPLAYED: 'Checkout form is not displayed!',
  ORDER_DETAILS_NOT_DISPLAYED: 'Order details are not displayed!',
  FINISH_PAGE_NOT_DISPLAYED: 'Finish page is not displayed!',
  THANK_YOU_MESSAGE_IS_WRONG: 'Thank You message is wrong'
};

module.exports = ErrorMessages;
