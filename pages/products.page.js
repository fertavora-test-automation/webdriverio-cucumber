const { ErrorMessages } = require('../utils');

const ProductsPage = {
  productItems: () => browser.$$('.inventory_item'),
  productByText: (text) => browser.$(`.inventory_item_name*=${text}`),

  areProductItemsDisplayed: () => {
    browser.waitUntil(
      () => ProductsPage.productItems().length > 0,
      undefined,
      ErrorMessages.PRODUCTS_NOT_DISPLAYED
    );

    return ProductsPage.productItems()[0].isDisplayed();
  },

  clickProductByText: (text) => ProductsPage.productByText(text).click()
};

module.exports = ProductsPage;
