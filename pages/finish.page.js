const Finish = {
  checkoutComplete: () => browser.$('.checkout_complete_container'),
  completeHeader: () => browser.$('.complete-header'),

  isThankYouMessageDisplayed: () => Finish.checkoutComplete().waitForDisplayed(),
  getHeaderText: () => Finish.completeHeader().getText(),
};

module.exports = Finish;
