const Overview = {
  buttonFinish: () => browser.$('.cart_button*=FINISH'),
  productsList: () => browser.$('.cart_list'),
  summaryInfo: () => browser.$('.summary_info'),

  areOrderDetailsDisplayed: () => {
    Overview.productsList().waitForDisplayed();
    Overview.summaryInfo().waitForDisplayed();
    return Overview.buttonFinish().waitForDisplayed();
  },

  clickFinishButton: () => Overview.buttonFinish().click()
};

module.exports = Overview;
