const { ErrorMessages } = require('../utils');

const ProductDetailsPage = {
  buttonBack: () => browser.$('.inventory_details_back_button'),
  buttonAddToCart: () => browser.$('.btn_primary.btn_inventory'),
  buttonRemove: () => browser.$('.btn_secondary.btn_inventory'),
  badgeShoppingCart: () => browser.$('.shopping_cart_container .shopping_cart_badge'),
  buttonGoToCart: () => browser.$('.shopping_cart_container'),

  clickButtonAddToCart: () => ProductDetailsPage.buttonAddToCart().click(),
  clickButtonGoToCart: () => ProductDetailsPage.buttonGoToCart().click(),

  isProductDetailsDisplayed: () => {
    ProductDetailsPage.buttonBack().waitForDisplayed(
      undefined,
      undefined,
      ErrorMessages.PRODUCT_DETAILS_BACK_BUTTON_NOT_DISPLAYED
    );
    return ProductDetailsPage.buttonAddToCart().waitForDisplayed(
      undefined,
      undefined,
      ErrorMessages.PRODUCT_DETAILS_ADD_TO_CART_BUTTON_NOT_DISPLAYED
    );
  },

  isCartBadgeDisplayed: () => ProductDetailsPage.badgeShoppingCart().waitForDisplayed(
    undefined,
    undefined,
    ErrorMessages.CART_BADGE_NOT_DISPLAYED
  ),
  isRemoveButtonDisplayed: () => ProductDetailsPage.buttonRemove().waitForDisplayed(
    undefined,
    undefined,
    ErrorMessages.REMOVE_BUTTON_NOT_DISPLAYED
  )
};

module.exports = ProductDetailsPage;
