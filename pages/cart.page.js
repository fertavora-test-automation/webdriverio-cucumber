const { ErrorMessages } = require('../utils');

const Cart = {
  buttonContinueShopping: () => browser.$('.btn_secondary*=Continue Shopping'),
  buttonCheckout: () => browser.$('.checkout_button'),

  isContinueShoppingButtonDisplayed: () => Cart.buttonContinueShopping().waitForDisplayed(
    undefined,
    undefined,
    ErrorMessages.CONTINUE_SHOPPING_BUTTON_NOT_DISPLAYED
  ),

  clickCheckoutButton: () => Cart.buttonCheckout().click()
};

module.exports = Cart;
