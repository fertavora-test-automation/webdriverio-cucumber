const Checkout = {
  inputFirstName: () => browser.$('#first-name'),
  inputLastName: () => browser.$('#last-name'),
  inputZipCode: () => browser.$('#postal-code'),
  buttonContinue: () => browser.$('.cart_button'),

  clickContinueButton: () => Checkout.buttonContinue().click(),

  fillCheckoutForm: (firstname, lastname, zipcode) => {
    Checkout.inputFirstName().setValue(firstname);
    Checkout.inputLastName().setValue(lastname);
    return Checkout.inputZipCode().setValue(zipcode);
  },

  isFormDisplayed: () => {
    Checkout.inputFirstName().waitForDisplayed();
    Checkout.inputLastName().waitForDisplayed();
    Checkout.inputZipCode().waitForDisplayed();
    return Checkout.buttonContinue().waitForDisplayed();
  }
};

module.exports = Checkout;
