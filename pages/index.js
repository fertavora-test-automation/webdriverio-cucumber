exports.LoginPage = require('./login.page');
exports.ProductsPage = require('./products.page');
exports.ProductDetailsPage = require('./product.details.page');
exports.Cart = require('./cart.page');
exports.Checkout = require('./checkout.page');
exports.Overview = require('./overview.page');
exports.Finish = require('./finish.page');
