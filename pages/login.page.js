
const LoginPage = {
  inputUsername: () => browser.$('#user-name'),
  inputPassword: () => browser.$('#password'),
  buttonLogin: () => browser.$('.btn_action'),

  signIn: (username, password) => {
    LoginPage.inputUsername().setValue(username);
    LoginPage.inputPassword().setValue(password);
    LoginPage.buttonLogin().click();
  }
};

module.exports = LoginPage;
